SMARTM_ChannelHandler = {}
SMARTM_ChannelHandler.__index = SMARTM_ChannelHandler

function SMARTM_ChannelHandler:Create(Variables, CommonWorker, HistoryHandler)
	local ChannelHandler = {}
	setmetatable(ChannelHandler, SMARTM_ChannelHandler)
	
	ChannelHandler.Variables = Variables
	ChannelHandler.CommonWorker = CommonWorker
	ChannelHandler.HistoryHandler = HistoryHandler
	
	ChannelHandler.LastMessage = 0
	
	ChannelHandler.hoChatFrame_MessageEventHandler = ChatFrame_MessageEventHandler
	--noinspection GlobalCreationOutsideO
	ChatFrame_MessageEventHandler = ChannelHandler.hfChatFrame_MessageEventHandler

	return ChannelHandler
end



-- used in original version for message formatting (moderation quick links), arg-stack not working correclty
--  in pandaria, so history filling within this method is deprecated
function SMARTM_ChannelHandler:HookChatMessage(this, FrameMessage, R, G, B, MessageID, AddToStart)
	local Result = FrameMessage
	local Message, Nickname, _, _, _, _, _, ChannelNumber, ChannelName, _, Identity, _ = arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12

	if self:IsDataValid(FrameMessage, Message, Nickname, ChannelNumber, ChannelName, Identity) then
		if self:CriticalSection(Identity) then
			local FormatedMessage = self.CommonWorker:FormatMessage(Message, ChannelName)
			self.HistoryHandler:ProcessChatHistory(Nickname, FormatedMessage)
		end
	end

	this:hoChatFrame_AddMessage(Result, R, G, B, MessageID, AddToStart)
end



-- used in original version for message formatting (moderation quick links), arg-stack not working correclty
--  in pandaria, so history filling within this method is deprecated
function SMARTM_ChannelHandler.HookChatMessageFunction(...)
	return _SMARTModeration.ChannelHandler:HookChatMessage(...)
end



function SMARTM_ChannelHandler:ProcessHistory(...)
	local Message, Nickname, _, _, _, _, _, ChannelNumber, ChannelName, _, Identity, _ = ...

	if self:IsDataValid(Message, Nickname, ChannelNumber, ChannelName, Identity) then
		if self:CriticalSection(Identity) then
			local FormatedMessage = self.CommonWorker:FormatMessage(Message, ChannelName)
			self.HistoryHandler:ProcessChatHistory(Nickname, FormatedMessage)
		end
	end
end



function SMARTM_ChannelHandler.hfChatFrame_MessageEventHandler(this, event, ...)
	_SMARTModeration.ChannelHandler:ProcessHistory(...)
	_SMARTModeration.ChannelHandler.hoChatFrame_MessageEventHandler(this, event, ...)

	-- used in original version for message formatting (moderation quick links), arg-stack not working correclty
	--  in pandaria, so history filling within this method is deprecated
	--if not this.hoChatFrame_AddMessage then
	--	this.hoChatFrame_AddMessage = this.AddMessage
	--	this.AddMessage = _SMARTModeration.ChannelHandler.HookChatMessageFunction
	--end
end



function SMARTM_ChannelHandler:CriticalSection(Identity)
	local Result = self.LastMessage ~= Identity
	
	self.LastMessage = Identity
	
	return Result
end



function SMARTM_ChannelHandler:IsModerated(ChannelNumber)
	local _, ChannelName = GetChannelName(ChannelNumber);			
	ChannelName = ChannelName:upper()
	
	if ChannelName == self.Variables.CHANNEL_LFG 
	or ChannelName == self.Variables.CHANNEL_WTC then	
		return true
	else
		return false
	end
end



function SMARTM_ChannelHandler:IsDataValid(Message, Nickname, ChannelNumber, ChannelName, Identity)
	if self.CommonWorker:IsEmpty(Identity) then
		return false
	end
	if Identity < self.LastMessage then
		return false
	end
	
	if self.CommonWorker:IsEmpty(Message) then
		return false
	end
	
	if self.CommonWorker:IsEmpty(Nickname) then
		return false
	end
	
	if self.CommonWorker:IsEmpty(ChannelNumber) then
		return false
	end
	if ChannelNumber == 0 then
		return false
	end
	if not self:IsModerated(ChannelNumber) then
		return false
	end
	
	if self.CommonWorker:IsEmpty(ChannelName) then
		return false
	end
	
	return true
end