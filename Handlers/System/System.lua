SMARTM_SystemHandler = {}
SMARTM_SystemHandler.__index = SMARTM_SystemHandler

function SMARTM_SystemHandler:Create(Variables, CommonWorker, HistoryHandler)
	local SystemHandler = {}
	setmetatable(SystemHandler, SMARTM_SystemHandler)
	
	SystemHandler.Variables = Variables
	SystemHandler.CommonWorker = CommonWorker
	SystemHandler.HistoryHandler = HistoryHandler
	
	SystemHandler.LastMessage = 0
	SystemHandler.FilterMode = _SMARTModeration.Variables.SYSFILTER_DIRECT
	
	ChatFrame_AddMessageEventFilter('CHAT_MSG_SYSTEM', SystemHandler.FilterFunction)
	
	return SystemHandler
end



--noinspection UnusedDef
function SMARTM_SystemHandler:Filter(this, event, ...)
	local  Message, _, _, _, _, _, _, _, _ ,_ , Identity = ...

	if Identity > self.LastMessage then
		local IsFiltered
		self.LastMessage = Identity
		
		--filter '.save' result
		if Message == self.Variables.FILTER_SAVED then
			IsFiltered = true
		end
		
		--filter and replace mute messages
		if (string.find(Message, 'Чат')) and
					(string.find(Message, 'заблокирован администратором')) and
					(string.find(Message, 'на')) and
					(string.find(Message, 'мин по причине:')) then
			local Counter = 1
			local Target, Reason, Time, Author = '', '', '', ''
		
			for WordSplit in string.gmatch(Message, '[^ ]+') do
				if Counter == 4 then
					Target = WordSplit:match("%[.+%]"):sub(2, -2)
				end
			
				if Counter == 9 then
					Time = WordSplit
					
					if (tonumber(Time)) and (tonumber(Time)) > 720 then
						Time = '720'
					end
				end

				if Counter == 7 then
					Author = WordSplit
				end

				if Counter == 13 then
					Reason = WordSplit
				end	

				if Counter > 13 then
					--noinspection StringConcatenationInLoops
					Reason = Reason .. ' ' .. WordSplit
				end
			
				Counter = Counter + 1
			end
			
			SendSystemMessage(self.Variables.TEXT_GMANNOUNCE:format(Author, Author, Target, Target, self.CommonWorker:FormatTime(Time), Reason))
			
			IsFiltered = true
		end
		
		--activate history filter
		if select(1, string.find(Message, 'История мутов персонажа ')) == 1 or select(1, string.find(Message, 'Character (.+) mute history:')) == 1 then
			self.HistoryHandler:ClearMuteHistory()
		
			self.FilterMode = self.Variables.SYSFILTER_HISTORY
			
			IsFiltered = true
		end

		--process history record
		if self.FilterMode == self.Variables.SYSFILTER_HISTORY then
			self.HistoryHandler:ProcessMuteHistory(Message)
			
			IsFiltered = true
		end
		
		--deactivate history filter
		if select(1, string.find(Message, 'Конец истории')) == 1 or select(1, string.find(Message, 'End of history')) == 1 then
			self.FilterMode = self.Variables.SYSFILTER_DIRECT
			
			self.HistoryHandler:ShowMuteHistory()
			
			IsFiltered = true
			
			TakeScreenshot()
		end
		
		--manual handle system messages
		if not IsFiltered then
			for Index = 1, NUM_CHAT_WINDOWS do
				local MessageTypes = {GetChatWindowMessages(_G['ChatFrame' .. Index]:GetID())};
				
				for _, MessageType in pairs(MessageTypes) do
					if MessageType == 'SYSTEM' then
						_G['ChatFrame' .. Index]:AddMessage(Message, 1, 1, 0);
						break
					end
				end
			end
		end
	end
	
	return true, ...
end



function SMARTM_SystemHandler.FilterFunction(...)
	return _SMARTModeration.SystemHandler:Filter(...)
end