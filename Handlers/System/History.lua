SMARTM_HistoryHandler = {}
SMARTM_HistoryHandler.__index = SMARTM_HistoryHandler

function SMARTM_HistoryHandler:Create(Variables, Rules, CommonWorker, InterfaceWorker)
	local HistoryHandler = {}
	setmetatable(HistoryHandler, SMARTM_HistoryHandler)
	
	HistoryHandler.Variables = Variables
	HistoryHandler.Rules = Rules
	HistoryHandler.CommonWorker = CommonWorker
	HistoryHandler.InterfaceWorker = InterfaceWorker
	
	self.ChatHistory = {}
	HistoryHandler:ClearMuteHistory()
	
	return HistoryHandler
end



function SMARTM_HistoryHandler:ClearMuteHistory()
	self.MuteStats = {}
	self.MuteHistory = {}
	self.MuteIndex = {}
	self.MuteRecord = {}
	
	self.ProcessMode = self.Variables.HISTORY_NONE
	self.StartProcessing = false
	
	self.UserAccount = nil
	self.UserName = nil
	
	self.InterfaceWorker:ClearMuteHistory()
end



function SMARTM_HistoryHandler:GetStat(ReasonCode, StatCode)
	if not self.MuteStats[ReasonCode] then
		return nil
	end
	
	return self.MuteStats[ReasonCode][StatCode] or nil
end



function SMARTM_HistoryHandler:Print(...)
	local Values = {...}
	local Result = ''
	
	for _, v in pairs(Values) do
		--noinspection StringConcatenationInLoops
		Result = Result .. v
	end

	self.InterfaceWorker:AddMuteHistory(Result)
end



function SMARTM_HistoryHandler:ShowMuteHistory()
	self.InterfaceWorker.Interface.CheckButtons:ClearCaptions()

	if #self.MuteIndex > 0 then
		self.CommonWorker:QuickSort(self.MuteIndex, 1, #self.MuteIndex)
		
		for _, Index in pairs(self.MuteIndex) do
			local Record = self.MuteHistory[Index]

			--noinspection UnusedDef
			local DateTime = self.CommonWorker:Unix2DateTime(Record.DateTime)

			--noinspection UnusedDef
			local TimeDiff = self.CommonWorker:FormatTimeDiff(self.CommonWorker:GetServerTime() - Record.DateTime)
			
			local Target
			if Record.Target == self.UserName then
				Target = string.format(self.Variables.TEXT_HISTORY_INTARGET, Record.Target)
			else
				Target = string.format(self.Variables.TEXT_HISTORY_OUTTARGET, Record.Target)
			end	
			
			if Record.Newname then
				Target = string.format(self.Variables.TEXT_HISTORY_RENAMED, Target, Record.Newname)
			end
			
			local MuteTime = self.CommonWorker:FormatTime(Record.MuteTime)
			
			self:Print(string.format(self.Variables.TEXT_HISTORY, DateTime, TimeDiff, Record.Author, Target, Record.Reason, MuteTime))
		end
	end
	
	for Index, _ in pairs(self.Rules.MuteList) do
		local Caption = ''
		local Day = self:GetStat(Index, self.Variables.HISTORY_STATS_DAY)
		local Week = self:GetStat(Index, self.Variables.HISTORY_STATS_WEEK)
		local Month = self:GetStat(Index, self.Variables.HISTORY_STATS_MONTH)
		local Year = self:GetStat(Index, self.Variables.HISTORY_STATS_YEAR)
		local All = self:GetStat(Index, self.Variables.HISTORY_STATS_ALL)
		
		if Day or Week or Month or Year or All then
			Caption = self.Variables.TEXT_CHECKBUTTON_STATS:format(
				Day or '0',
				Week or '0',
				Month or '0',
				Year or '0',
				All or '0'
			)
		end
			
		self.InterfaceWorker.Interface.CheckButtons:SetCaption(Index, Caption)
	end
end



function SMARTM_HistoryHandler:ProcessMuteRecord(Record)
	if Record and Record.Mode and Record.DateTime and Record.Target and Record.MuteAccount and Record.MuteTime and Record.Author and Record.Reason then
		if not self.MuteHistory[Record.DateTime] then
			table.insert(self.MuteIndex, Record.DateTime)
			self.MuteHistory[Record.DateTime] = Record
			
			local UpperReason = Record.Reason:upper()
			local TimeDiff = self.CommonWorker:GetServerTime() - Record.DateTime
			for ReasonCode, _ in pairs(self.Rules.MuteList) do
				if string.find(UpperReason, self.Rules:GetShortReason(ReasonCode):upper()) then
					if not self.MuteStats[ReasonCode] then
						self.MuteStats[ReasonCode] = {}
					end
					
					for _, Value in pairs(self.Variables.HISTORY_STATS_TABLE) do
						if TimeDiff < Value[self.Variables.HISTORY_STATS_TABLE_SECONDS] then
							if not self.MuteStats[ReasonCode][Value[self.Variables.HISTORY_STATS_TABLE_INTERVAL]] then
								self.MuteStats[ReasonCode][Value[self.Variables.HISTORY_STATS_TABLE_INTERVAL]] = 1
							else
								self.MuteStats[ReasonCode][Value[self.Variables.HISTORY_STATS_TABLE_INTERVAL]] = self.MuteStats[ReasonCode][Value[self.Variables.HISTORY_STATS_TABLE_INTERVAL]] + 1
							end
						end
					end
					
					if not self.MuteStats[ReasonCode][self.Variables.HISTORY_STATS_ALL] then
						self.MuteStats[ReasonCode][self.Variables.HISTORY_STATS_ALL] = 1
					else
						self.MuteStats[ReasonCode][self.Variables.HISTORY_STATS_ALL] = self.MuteStats[ReasonCode][self.Variables.HISTORY_STATS_ALL] + 1
					end
				end
			end
		end
	else
		if GetLocale() == "ruRU" then self:Print('Внимание! Обнаружена неполная запись истории, данные могут быть некорректы! Предоставьте, пожалуйста, скриншот от этой строки...') else self:Print('Attention! An incomplete history record was detected, the data may be incorrect! Please provide a screenshot from this line...') end
		for i, v in pairs(Record) do
			self:Print(i, '=', v)
		end
		if GetLocale() == "ruRU" then self:Print('... до этой разработчику. Спасибо.') else self:Print('... to this developer. Thank.') end
	end
end



function SMARTM_HistoryHandler:ProcessMuteHistory(HistoryLine)
	if HistoryLine == self.Variables.PATTERN_HISTORY_DELIMITER then
		if self.MuteRecord.Mode ~= self.Variables.HISTORY_DELETED then
			if self.StartProcessing then
				self:ProcessMuteRecord(self.MuteRecord)
			else
				self.StartProcessing = true
			end
		end
		
		self.MuteRecord = {}
		self.MuteRecord.Mode = nil 
	else
		if not self.UserName then
			self.UserName = HistoryLine:match(self.Variables.PATTERN_HISTORY_CHARACTER)
			
			if self.UserName then
				self.ProcessMode = self.Variables.HISTORY_CHARACTER
				self.StartProcessing = false
			end
		end
		
		if not self.UserAccount then
			self.UserAccount = HistoryLine:match(self.Variables.PATTERN_HISTORY_ACCOUNT)
			
			if self.UserAccount then
				self.ProcessMode = self.Variables.HISTORY_ACCOUNT
				self.StartProcessing = false
			end
		end
		
		if self.MuteRecord.Mode ~= self.Variables.HISTORY_DELETED then
			self.MuteRecord.Mode = self.ProcessMode
		end
		
		if HistoryLine:match(self.Variables.PATTERN_HISTORY_DELETED) then
			self.MuteRecord.Mode = self.Variables.HISTORY_DELETED
		end
		
		self.MuteRecord.DateTime = self.CommonWorker:ParseTime(HistoryLine) or self.MuteRecord.DateTime
		self.MuteRecord.Target = HistoryLine:match(self.Variables.PATTERN_HISTORY_TARGET) or self.MuteRecord.Target
		self.MuteRecord.MuteAccount = HistoryLine:match(self.Variables.PATTERN_HISTORY_MUTEACCOUNT) or self.MuteRecord.MuteAccount or self.UserAccount
		local Target, Newname = HistoryLine:match(self.Variables.PATTERN_HISTORY_NEWNAME)
		if Target and Newname then
			self.MuteRecord.Target = Target
			self.MuteRecord.Newname = Newname
		end
		
		self.MuteRecord.MuteTime = HistoryLine:match(self.Variables.PATTERN_HISTORY_MUTETIME) or self.MuteRecord.MuteTime
		self.MuteRecord.Author = HistoryLine:match(self.Variables.PATTERN_HISTORY_AUTHOR) or self.MuteRecord.Author
		self.MuteRecord.Reason = HistoryLine:match(self.Variables.PATTERN_HISTORY_REASON) or self.MuteRecord.Reason
	end
end



function SMARTM_HistoryHandler:ProcessChatHistory(Nickname, Message)
	Nickname = self.CommonWorker:CleanupNickname(Nickname)

	local Now = time()
	if not self.ChatHistory[Nickname] then
		self.ChatHistory[Nickname] = {}
	end
	if not self.ChatHistory[Nickname][Now] then
		self.ChatHistory[Nickname][Now] = {}
	end
	table.insert(self.ChatHistory[Nickname][Now], Message)
end