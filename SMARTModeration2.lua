SMARTModeration2 = {}
SMARTModeration2.__index = SMARTModeration2

function SMARTModeration2:Create()
	--noinspection ParameterSelf
	local self = {}
	setmetatable(self, SMARTModeration2)

	self.Variables = SMARTM_Variables:Create()
	self.Rules = SMARTM_Rules:Create(self.Variables)

	self.Interface = SMARTM_Interface:Create(self.Rules)

	self.CommonWorker = SMARTM_CommonWorker:Create(self.Variables)

	self.MuteWorker = SMARTM_MuteWorker:Create(self.Variables, self.Rules, self.CommonWorker)

	self.InterfaceWorker = SMARTM_InterfaceWorker:Create(self.Interface, self.Variables, self.Rules, self.MuteWorker, self.CommonWorker)
	
	return self
end



function SMARTModeration2:Init()
	SetCVar('profanityFilter', 0)
	if (BNSetMatureLanguageFilter) then
		BNSetMatureLanguageFilter(false)
	end

	self.CommandHandler = SMARTM_CommandHandler:Create()

	self.HistoryHandler = SMARTM_HistoryHandler:Create(self.Variables, self.Rules, self.CommonWorker, self.InterfaceWorker)
	self.InterfaceWorker:SetHistoryHandler(self.HistoryHandler)

	self.SystemHandler = SMARTM_SystemHandler:Create(self.Variables, self.CommonWorker, self.HistoryHandler)

	self.ChannelHandler = SMARTM_ChannelHandler:Create(self.Variables, self.CommonWorker, self.HistoryHandler)
end



_SMARTModeration = SMARTModeration2:Create()
_SMARTModeration:Init()

--_SMARTModeration.InterfaceWorker:Open('Anakosta', _SMARTModeration.Variables.MODE_WARN, _SMARTModeration.Variables.MUTE_CAPS, 1)
--_SMARTModeration.InterfaceWorker:Open('Морория', _SMARTModeration.Variables.MODE_WARN, _SMARTModeration.Variables.MUTE_CAPS, 1)
--_SMARTModeration.InterfaceWorker:Open('Jeksparo', _SMARTModeration.Variables.MODE_WARN, _SMARTModeration.Variables.MUTE_CAPS, 1)