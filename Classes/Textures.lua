SMARTM_Textures = {}
SMARTM_Textures.__index = SMARTM_Textures

function SMARTM_Textures:Create()
	local Textures = {}
	setmetatable(Textures, SMARTM_Textures)
	
	return Textures
end



function SMARTM_Textures:Load(Frame, Path, Style)
	Style = Style or 'BACKGROUND'
	
	local Texture = Frame:CreateTexture(nil, Style)
	Texture:SetTexture(Path)
	
	return Texture
end



function SMARTM_Textures:Background(Frame)
	local Texture = self:Load(Frame, 'Interface\\Addons\\SMARTModeration2\\Textures\\Background')
	Texture:SetTexCoord(0, 1, 0, 0.68359375)
	Texture:SetAllPoints(Frame)
	Frame.texture = Texture
end



function SMARTM_Textures:ActionButton(Frame)
	local Texture = self:Load(Frame, 'Interface\\Addons\\SMARTModeration2\\Textures\\ActionButtonNormal')
	Texture:SetTexCoord(0, 0.8828125, 0, 0.5625)
	Texture:SetAllPoints(Frame)
	Frame:SetNormalTexture(Texture)
	
	local Texture = self:Load(Frame, 'Interface\\Addons\\SMARTModeration2\\Textures\\ActionButtonHighlight')
	Texture:SetTexCoord(0, 0.8828125, 0, 0.5625)
	Texture:SetAllPoints(Frame)
	Frame:SetHighlightTexture(Texture)
	
	local Texture = self:Load(Frame, 'Interface\\Addons\\SMARTModeration2\\Textures\\ActionButtonPushed')
	Texture:SetTexCoord(0, 0.8828125, 0, 0.5625)
	Texture:SetAllPoints(Frame)
	Frame:SetPushedTexture(Texture)
end



function SMARTM_Textures:ActionButtonHighlight(Frame)
	local Texture = self:Load(Frame, 'Interface\\Addons\\SMARTModeration2\\Textures\\ActionButtonPushed')
	Texture:SetTexCoord(0, 0.8828125, 0, 0.5625)
	Texture:SetAllPoints(Frame)
	Frame:SetNormalTexture(Texture)
	
	local Texture = self:Load(Frame, 'Interface\\Addons\\SMARTModeration2\\Textures\\ActionButtonHighlight')
	Texture:SetTexCoord(0, 0.8828125, 0, 0.5625)
	Texture:SetAllPoints(Frame)
	Frame:SetHighlightTexture(Texture)
	
	local Texture = self:Load(Frame, 'Interface\\Addons\\SMARTModeration2\\Textures\\ActionButtonNormal')
	Texture:SetTexCoord(0, 0.8828125, 0, 0.5625)
	Texture:SetAllPoints(Frame)
	Frame:SetPushedTexture(Texture)
end



function SMARTM_Textures:CheckButton(Frame)
	local Texture = self:Load(Frame, 'Interface\\Addons\\SMARTModeration2\\Textures\\MuteSelectionButtonNormal')
	Texture:SetTexCoord(0, 1, 0, 0.5625)
	Texture:SetAllPoints(Frame)
	Frame:SetNormalTexture(Texture)
	
	local Texture = self:Load(Frame, 'Interface\\Addons\\SMARTModeration2\\Textures\\MuteSelectionButtonHighlight')
	Texture:SetTexCoord(0, 1, 0, 0.5625)
	Texture:SetAllPoints(Frame)
	Frame:SetHighlightTexture(Texture)
	
	local Texture = self:Load(Frame, 'Interface\\Addons\\SMARTModeration2\\Textures\\MuteSelectionButtonPushed')
	Texture:SetTexCoord(0, 1, 0, 0.5625)
	Texture:SetAllPoints(Frame)
	Frame:SetPushedTexture(Texture)
	
	
	local Texture = self:Load(Frame, 'Interface\\Addons\\SMARTModeration2\\Textures\\MuteSelectionButtonPushed')
	Texture:SetTexCoord(0, 1, 0, 0.5625)
	Texture:SetAllPoints(Frame)
	Frame:SetCheckedTexture(Texture)
end



function SMARTM_Textures:CheckButtonIcon(Frame, Path)
	local Texture = self:Load(Frame, Path)
	Texture:SetTexCoord(0.1, 0.9, 0.1, 0.9)
	Texture:SetAllPoints(Frame)
	Frame.texture = Texture
end



function SMARTM_Textures:CloseButton(Frame)
	local Texture = self:Load(Frame, 'Interface\\Buttons\\ui-panel-minimizebutton-up')
	Texture:SetTexCoord(0, 1, 0, 1)
	Texture:SetAllPoints(Frame)
	Frame:SetNormalTexture(Texture)
	
	local Texture = self:Load(Frame, 'Interface\\Buttons\\ui-panel-minimizebutton-highlight')
	Texture:SetTexCoord(0, 1, 0, 1)
	Texture:SetAllPoints(Frame)
	Frame:SetHighlightTexture(Texture)
	
	local Texture = self:Load(Frame, 'Interface\\Buttons\\ui-panel-minimizebutton-down')
	Texture:SetTexCoord(0, 1, 0, 1)
	Texture:SetAllPoints(Frame)
	Frame:SetPushedTexture(Texture)
	
	
	local Texture = self:Load(Frame, 'Interface\\Buttons\\ui-panel-minimizebutton-disabled')
	Texture:SetTexCoord(0, 1, 0, 1)
	Texture:SetAllPoints(Frame)
	Frame:SetDisabledTexture(Texture)
end