SMARTM_HistoryFrame = {}
SMARTM_HistoryFrame.__index = SMARTM_HistoryFrame

--noinspection LuaOverlyLongMethod
function SMARTM_HistoryFrame:Create(Interface, ParentFrame)
	local HistoryFrame = {}
	setmetatable(HistoryFrame, SMARTM_HistoryFrame)
	
	HistoryFrame.Interface = Interface
	HistoryFrame.ParentFrame = ParentFrame
	
	HistoryFrame.HolderFrame = CreateFrame('Frame', nil, HistoryFrame.ParentFrame)
	HistoryFrame.HolderFrame:SetSize(730, 350)
	HistoryFrame.HolderFrame:SetFrameStrata('TOOLTIP')	
	HistoryFrame.HolderFrame:SetPoint('TOPLEFT', HistoryFrame.ParentFrame, 'TOPLEFT', 0, -300)
	HistoryFrame.HolderFrame:SetBackdrop({
		bgFile 		= 'Interface\\DialogFrame\\UI-DialogBox-Background',
		edgeFile	= 'Interface\\LFGFrame\\LFGBorder',
		tile 		= true,
		tileSize 	= 32,
		edgeSize 	= 32,
		insets 		= {left = 8, right = 8, top = 8, bottom = 8}
	}) 			  
	HistoryFrame.HolderFrame:SetBackdropColor(0, 0, 0, 1)
	HistoryFrame.HolderFrame:EnableMouseWheel(true)
	
	HistoryFrame.MessageFrame = CreateFrame('ScrollingMessageFrame', nil, HistoryFrame.HolderFrame)
	HistoryFrame.MessageFrame:SetSize(HistoryFrame.HolderFrame:GetWidth() - 50, HistoryFrame.HolderFrame:GetHeight() - 30)
	HistoryFrame.MessageFrame:SetPoint('TOPLEFT', 15, -12)
	HistoryFrame.MessageFrame:SetFont('Fonts\\ARIALN.TTF', 12)
	HistoryFrame.MessageFrame:SetTextColor(1, 1, 1, 1)
	HistoryFrame.MessageFrame:SetJustifyH('LEFT')
	HistoryFrame.MessageFrame:SetHyperlinksEnabled(true)
	HistoryFrame.MessageFrame:SetFading(false)
	HistoryFrame.MessageFrame:SetMaxLines(1000)
	HistoryFrame.MessageFrame:SetIndentedWordWrap(true)

	HistoryFrame.ScrollBar = CreateFrame('Slider', nil, HistoryFrame.HolderFrame, 'UIPanelScrollBarTemplate')
	HistoryFrame.ScrollBar:ClearAllPoints()
	HistoryFrame.ScrollBar:SetPoint('RIGHT', HistoryFrame.HolderFrame, 'RIGHT', -10, 0)
	HistoryFrame.ScrollBar:SetSize(30, HistoryFrame.HolderFrame:GetHeight() - 68)
	HistoryFrame.ScrollBar:SetMinMaxValues(0, HistoryFrame.MessageFrame:GetNumMessages())
	HistoryFrame.ScrollBar:SetValueStep(1)	
	HistoryFrame.ScrollBar.scrollStep = 1 
	HistoryFrame.ScrollBar:SetScript('OnValueChanged', function(this, value)
		local HolderFrame = this:GetParent()
		local FrameObjects = {HolderFrame:GetChildren()}
		
		for _, FrameObject in ipairs(FrameObjects) do
			if FrameObject:GetObjectType() == 'ScrollingMessageFrame' then
				FrameObject:SetScrollOffset(select(2, this:GetMinMaxValues()) - value)
			end
		end
	 end)
	HistoryFrame.ScrollBar:SetValue(select(2, HistoryFrame.ScrollBar:GetMinMaxValues()))
	
	HistoryFrame.HolderFrame:SetScript('OnMouseWheel', function(this, delta)
		local HolderFrame = this;
		local FrameObjects = {HolderFrame:GetChildren()}
		
		local ScrollBar
		local MessageFrame
		for _, FrameObject in ipairs(FrameObjects) do
			if FrameObject:GetObjectType() == 'Slider' then
				ScrollBar = FrameObject
			end
			
			if FrameObject:GetObjectType() == 'ScrollingMessageFrame' then
				MessageFrame = FrameObject
			end
		end
		
		ScrollBar:SetMinMaxValues(MessageFrame:GetNumLinesDisplayed(), MessageFrame:GetNumMessages())
		
		local cur_val = ScrollBar:GetValue()
		local min_val, max_val = ScrollBar:GetMinMaxValues()
		if delta < 0 and cur_val < max_val then
			cur_val = math.min(max_val, cur_val + 1)
			ScrollBar:SetValue(cur_val)			
		elseif delta > 0 and cur_val > min_val then
			cur_val = math.max(min_val, cur_val - 1)
			ScrollBar:SetValue(cur_val)		
		end	
	 end)
	
	return HistoryFrame
end



function SMARTM_HistoryFrame:Push(Message)
	self.MessageFrame:AddMessage(Message)
	
	self.ScrollBar:SetMinMaxValues(self.MessageFrame:GetNumLinesDisplayed(), self.MessageFrame:GetNumMessages())
	
	self.MessageFrame:ScrollToBottom()
	self.ScrollBar:SetValue(select(2, self.ScrollBar:GetMinMaxValues()))
end



function SMARTM_HistoryFrame:Clear()
	self.MessageFrame:Clear()
end