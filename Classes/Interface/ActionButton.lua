SMARTM_ActionButton = {}
SMARTM_ActionButton.__index = SMARTM_ActionButton

function SMARTM_ActionButton:Create(Interface, Textures, ParentFrame)
	local ActionButton = {}
	setmetatable(ActionButton, SMARTM_ActionButton)
	
	ActionButton.Interface = Interface
	ActionButton.Textures = Textures
	ActionButton.ParentFrame = ParentFrame
	
	ActionButton.ButtonFrame = CreateFrame('Button', nil, ActionButton.ParentFrame)
	ActionButton.ButtonFrame:SetSize(226, 36)
	ActionButton.Textures:ActionButton(ActionButton.ButtonFrame)
	
	return ActionButton
end



function SMARTM_ActionButton:DrawInit(Caption, OffsetX, OffsetY)
	self.ButtonFrame:SetPoint('TOPLEFT', OffsetX, OffsetY)
	self.ButtonFrame:SetText(Caption)
	
	self.Caption = self.ButtonFrame:CreateFontString(nil, 'OVERLAY', 'GameFontNormal')
	self.Caption:SetPoint('TOPLEFT', 12, -6)
	self.Caption:SetText(Caption)
	
	self.ButtonFrame:Show()
end