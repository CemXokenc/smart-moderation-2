SMARTM_Interface = {}
SMARTM_Interface.__index = SMARTM_Interface

--noinspection LuaOverlyLongMethod
function SMARTM_Interface:Create(Rules)
	local Interface = {}
	setmetatable(Interface, SMARTM_Interface)
	
	Interface.Textures = SMARTM_Textures:Create()
	
	--creating main interface frame
	Interface.MainFrame = CreateFrame('Frame', nil, nil)
	Interface.MainFrame:SetFrameStrata('TOOLTIP')
	Interface.MainFrame:SetSize(1024, 700) 
	Interface.MainFrame:SetScale(0.85)
	Interface.MainFrame:SetPoint('CENTER', 0, 0)
	Interface.MainFrame:Hide()
	Interface.MainFrame:EnableMouse(true)
	Interface.MainFrame:SetMovable(true)
	Interface.MainFrame:SetUserPlaced(true)
	Interface.MainFrame:RegisterForDrag("LeftButton")
	Interface.MainFrame:SetScript("OnDragStart", Interface.MainFrame.StartMoving)
	Interface.MainFrame:SetScript("OnDragStop", Interface.MainFrame.StopMovingOrSizing)
	Interface.Textures:Background(Interface.MainFrame)
	
	--creating left block of checkbuttons
	Interface.CheckButtons = SMARTM_CheckButtons:Create(Interface, Interface.Textures, Rules)
	Interface.CheckButtons:DrawInit(15, -20, -22)
	
	--creating right block holder
	Interface.MuteModeFrame = CreateFrame('Frame', nil, Interface.MainFrame)
	Interface.MuteModeFrame:SetFrameStrata('TOOLTIP')
	Interface.MuteModeFrame:SetSize(741, 700) 
	Interface.MuteModeFrame:SetPoint('TOPLEFT', 284, 0)
	
	--creating nickname text
	Interface.Nickname = Interface.MuteModeFrame:CreateFontString(nil, 'OVERLAY', 'GameFontNormal')	
	Interface.Nickname:SetPoint('TOP', 0, -15)
	if GetLocale() == "ruRU" then Interface.Nickname:SetText('Здесь могла быть Ваша реклама!') else Interface.Nickname:SetText('Your ad could be here!') end
	Interface.Nickname:SetFont('Fonts\\ARIALN.TTF', 36)
	
	--creating time caption
	local Caption = Interface.MuteModeFrame:CreateFontString(nil, 'OVERLAY', 'GameFontNormal')	
	Caption:SetPoint('TOPLEFT', 7, -47)
	if GetLocale() == "ruRU" then Caption:SetText('Время:') else Caption:SetText('Time:') end
	Caption:SetFont('Fonts\\ARIALN.TTF', 18)

	--creating time editbox
	Interface.TimeEditBox = CreateFrame('EditBox', nil, Interface.MuteModeFrame)
	Interface.TimeEditBox:SetFontObject('GameFontHighlight')
	Interface.TimeEditBox:SetSize(38, 25)
	Interface.TimeEditBox:SetPoint('TOPLEFT', 60, -45)
	Interface.TimeEditBox:SetMaxLetters(3)
	Interface.TimeEditBox:SetTextInsets(5, 1, 1, 1)
	Interface.TimeEditBox:SetScript('OnChar', function(self)
		self:SetText(self:GetText():match('%d+') or '1')
		if tonumber(self:GetText()) < 1 then
			self:SetText(1)
		end
		if tonumber(self:GetText()) > 720 then
			self:SetText(720)
		end
	end)
	Interface.TimeEditBox:SetBackdrop({
		bgFile 		= 'Interface\\DialogFrame\\UI-DialogBox-Background',
		edgeFile	= 'Interface/Tooltips/UI-Tooltip-Border',
		tile 		= true,
		tileSize 	= 8,
		edgeSize 	= 8,
		insets 		= {left = 2, right = 2, top = 2, bottom = 2}
	})

	--creating reason caption
	local Caption = Interface.MuteModeFrame:CreateFontString(nil, 'OVERLAY', 'GameFontNormal')
	Caption:SetPoint('TOPLEFT', 102, -47)
	if GetLocale() == "ruRU" then Caption:SetText('Причина:') else Caption:SetText('Reason:') end
	Caption:SetFont('Fonts\\ARIALN.TTF', 18)

	--creating reason editbox
	Interface.ReasonEditBox = CreateFrame('EditBox', nil, Interface.MuteModeFrame)
	Interface.ReasonEditBox:SetFontObject('GameFontHighlight')
	Interface.ReasonEditBox:SetSize(553, 25)
	Interface.ReasonEditBox:SetPoint('TOPLEFT', 170, -45)
	Interface.ReasonEditBox:SetMaxLetters(128)
	Interface.ReasonEditBox:SetTextInsets(5, 1, 1, 1)
	Interface.ReasonEditBox:SetBackdrop({
		bgFile 		= 'Interface\\DialogFrame\\UI-DialogBox-Background',
		edgeFile	= 'Interface/Tooltips/UI-Tooltip-Border',
		tile 		= true,
		tileSize 	= 8,
		edgeSize 	= 8,
		insets 		= {left = 2, right = 2, top = 2, bottom = 2}
	})
	if GetLocale() == "ruRU" then Interface.ReasonEditBox:SetText('Рекламное место сдаётся!') else Interface.ReasonEditBox:SetText('Rental of advertising space!') end

	--creating chat history frame
	Interface.ChatFrame = SMARTM_ChatFrame:Create(Interface, Interface.MuteModeFrame)

	--creating mute history frame
	Interface.HistoryFrame = SMARTM_HistoryFrame:Create(Interface, Interface.MuteModeFrame)

	--creating quiet mute button
	Interface.QuietMuteButton = SMARTM_ActionButton:Create(Interface, Interface.Textures, Interface.MuteModeFrame)
	if GetLocale() == "ruRU" then Interface.QuietMuteButton:DrawInit('Блокировка без аннонса', 7, -647) else Interface.QuietMuteButton:DrawInit('Silent blocking', 7, -647) end

	--creating standart mute button
	Interface.StandartMuteButton = SMARTM_ActionButton:Create(Interface, Interface.Textures, Interface.MuteModeFrame)
	if GetLocale() == "ruRU" then Interface.StandartMuteButton:DrawInit('Заблокировать чат!', 251, -647) else Interface.StandartMuteButton:DrawInit('Block chat!', 251, -647) end

	--creating warn mute button
	Interface.WarnMuteButton = SMARTM_ActionButton:Create(Interface, Interface.Textures, Interface.MuteModeFrame)
	if GetLocale() == "ruRU" then Interface.WarnMuteButton:DrawInit('Предупредить игрока', 496, -647) else Interface.WarnMuteButton:DrawInit('Warn player', 496, -647) end

	--creating close button
	Interface.CloseButton = CreateFrame('Button', nil, Interface.MuteModeFrame, 'UIPanelButtonTemplate')
	Interface.CloseButton:SetSize(40, 40)
	Interface.CloseButton:SetPoint('TOPRIGHT', 14, 14)
	Interface.Textures:CloseButton(Interface.CloseButton)

	return Interface
end



function SMARTM_Interface:ActionButtonHighlight(Button)
	Button.ButtonFrame:SetAlpha(1)
	self.Textures:ActionButtonHighlight(Button.ButtonFrame)
end



function SMARTM_Interface:ActionButtonShadow(Button)
	Button.ButtonFrame:SetAlpha(0.75)
	self.Textures:ActionButton(Button.ButtonFrame)
end



function SMARTM_Interface:ActionButtonNormal(Button)
	Button.ButtonFrame:SetAlpha(1)
	self.Textures:ActionButton(Button.ButtonFrame)
end
