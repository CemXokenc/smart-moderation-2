SMARTM_MuteWorker = {}
SMARTM_MuteWorker.__index = SMARTM_MuteWorker

function SMARTM_MuteWorker:Create(Variables, Rules, CommonWorker)
	local MuteWorker = {}
	setmetatable(MuteWorker, SMARTM_MuteWorker)
	
	MuteWorker.Variables = Variables
	MuteWorker.Rules = Rules
	MuteWorker.CommonWorker = CommonWorker
	
	return MuteWorker
end



function SMARTM_MuteWorker:Action(Action, Target, Time, Reason, ReasonCode)
	local MuteCommand = self.Variables.CMD_MUTE:format(Target, Time, Reason)

	if self.Variables.DebugMode == self.Variables.DEBUG_NONE then
		self.CommonWorker:SendCommand(MuteCommand)
	else
		print('SendCommand', MuteCommand)
	end
	
	if ReasonCode > 0 then
		self.CommonWorker:SendWhisper(Target, self.Rules:GetLongReason(ReasonCode))
	end
	
	if Action == self.Variables.ACTION_WARN then
		self.CommonWorker:SendWhisper(Target, self.Variables.TEXT_WARN)
	else
		if Action ~= self.Variables.ACTION_QUIET then
			self.CommonWorker:SendAnnounce(self.Variables.TEXT_ANNOUNCE:format(Target, self.CommonWorker:FormatTime(Time)))
		end
	end
end