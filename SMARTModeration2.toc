## Interface: 30300
## Title:|cFFFF9900[|r|cFF0099CCSM|r|cFFFF9900ART|r|cFF0099CC]|r |cFFCC0000Moderation|r|cFF0099CC2|r
## Author: Merci aka loTEDve
## Version: 2.0.0
## Notes: Аддон для модерации чата.
## Notes-ruRU: Аддон для модерации чата.
## eMail: loTEDve@GMail.CoM
## URL: http://loTEDve.ME/
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: SMARTM

Variables\Variables.lua
Variables\Rules.lua

Classes\Textures.lua

Classes\Interface\CheckButtons.lua
Classes\Interface\ChatFrame.lua
Classes\Interface\HistoryFrame.lua
Classes\Interface\ActionButton.lua
Classes\Interface\Interface.lua

Workers\Common.lua
Workers\Mute.lua
Workers\Interface.lua

Handlers\System\History.lua
Handlers\System\System.lua

Handlers\Channel.lua

Handlers\Command.lua



SMARTModeration2.lua