SMARTM_Rules = {}
SMARTM_Rules.__index = SMARTM_Rules

function SMARTM_Rules:Create(Variables)
	local Rules = {}
	setmetatable(Rules, SMARTM_Rules)
	
	Rules.Variables = Variables
	
	Rules.Active = 0
	
	Rules.MuteList = {}
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_TRADE] = 
		{
			30,
			'Торговля не в WTC',
			'Торговля в ненадлежащем канале',
			'Нарушение П/П 1.14: "Торговля в ненадлежащем канале". Все торговые операции проводятся в канале WTC (команда "/join WTC" без кавычек).',
			'Interface\\Icons\\INV_Misc_Coin_01',
			365,
			3
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_TRADE][2] = "Trading outside the WTC" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_OFFTOPIC] = 
		{
			30,
			'Оффтопик в WTC',
			'Оффтопик в торговом канале',
			'Нарушение П/П 1.16: "Оффтопик в торговом канале". В канале WTC разрешаются только сообщения, относящиеся к торговым операциям.',
			'Interface\\Icons\\INV_Misc_GroupLooking',
			90,
			3
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_OFFTOPIC][2] = "Offtopic in WTC" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_OVERPOST] = 
		{
			30,
			'Одинаковые сообщения',
			'Злоупотребление одинаковыми сообщениями',
			'Нарушение П/П 1.11: "Использование одинаковых сообщений в одном канале". Допускается только два одинаковых сообщения в канале за одну минуту.',
			'Interface\\Icons\\INV_Misc_PocketWatch_01',
			90,
			3
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_OVERPOST][2] = "Identical messages" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_CAPS] = 
		{
			30,
			'Заглавные буквы',
			'Злоупотребление заглавными буквами',
			'Нарушение П/П 1.9: "Злоупотребление заглавными буквами в сообщении". Не используйте в своих сообщениях СЛИШКОМ МНОГО ЗАГЛАВНЫХ БУКВ.',
			'Interface\\Icons\\Ability_Hunter_MastersCall',
			180,
			3
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_CAPS][2] = "CAPS" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_LONG] = 
		{
			30,
			'Больше двух строк',
			'Злоупотребление количеством сообщений',
			'Нарушение П/П 1.17: "Использование сообщения, разбитого на несколько частей". Постарайтесь уложиться в два сообщения за одну минуту.',
			'Interface\\Icons\\INV_MISC_NOTE_06',
			180,
			3
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_LONG][2] = "More than two lines (automatically)" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_SPAM] = 
		{
			30,
			'Реклама в любом виде',
			'Рассылка коммерческой и иной рекламы',
			'Нарушение П/П 1.6: "Спам в любом виде". Любая рассылка рекламы, в том числе и некоммерческой, запрещена.',
			'Interface\\Icons\\INV_Letter_06',
			365,
			1
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_SPAM][2] = "SPAM" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_LEXIS] = 
		{
			60,
			'Мат, оскорбления, клевета',
			'Мат, оскорбления игроков, клевета',
			'Нарушение П/П 1.4: "Нецензурная лексика, клевета, оскорбления в адрес игроков". Постарайтесь воздержаться от подобных высказываний.',
			'Interface\\Icons\\INV_Misc_Statue_06',
			365,
			5
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_LEXIS][2] = "Abusive language" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_HIDDEN] = 
		{
			30,
			'Завуалированный мат',
			'Использование завуалированного мата',
			'Нарушение П/П 1.15: "Использование завуалированного мата". Запрещено использование нецензурной лексики в любом виде.',
			'Interface\\Icons\\Ability_Ambush',
			365,
			3
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_HIDDEN][2] = "Hidden abusive language" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_PROVOKE] = 
		{
			120,
			'Провокационное поведение',
			'Провокация других игроков, флуд, бунт',
			'Нарушение П/П 1.12: "Провокационное поведение, вызывающее конфликты, массовый флуд, бунт". При рецидиве возможны более серьезные санкции.',
			'Interface\\Icons\\INV_Inscription_TarotDeath',
			-1,
			7
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_PROVOKE][2] = "Provocation, flood, riot" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_ANGER] = 
		{
			720,
			'Угрозы, призывы к насилию',
			'Угрозы, пропаганда, призывы к насилию',
			'Нарушение П/П 1.5: "Пропаганда, призывы к насилию, разглашение личных данных, угрозы". Постарайтесь воздержаться от подобных высказываний.',
			'Interface\\Icons\\Ability_CheapShot',
			-1,
			1
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_ANGER][2] = "Threats, calls for violence" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_NOOB] = 
		{
			30,
			'Надоедливые просьбы',
			'Надоедливые и настойчивые просьбы',
			'Нарушение П/П 1.1: "Надоедливые и настойчивые просьбы или требования к администрации или другим игрокам". Будьте терпеливее...',
			'Interface\\Icons\\Ability_Mage_Burnout',
			30,
			3
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_NOOB][2] = "Annoying requests" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_ADMIN] = 
		{
			240,
			'Оскорбление Администрации',
			'Оскорбление Администрации',
			'Нарушение П/П 1.3: "Оскорбления Администрации (в любом канале)". Оскорбления в адрес Администрации сервера запрещены во всех каналах.',
			'Interface\\Icons\\Ability_Mount_BigBlizzardBear',
			365,
			3
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_ADMIN][2] = "Insulting the Administration" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_IMAGES] = 
		{
			30,
			'Использование картинок',
			'Использование картинок в общем чате',
			'Нарушение П/П 1.8: "Использование картинок в мировом чате (к примеру {череп})". Не используйте картинки для выделения своего текста.',
			'Interface\\Icons\\Ability_Mount_RidingHorse',
			365,
			1
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_IMAGES][2] = "Images" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_LANG] = 
		{
			30,
			'Язык своей расы',
			'Общение в общем чате на языке своей расы',
			'Нарушение П/П 1.2: "Общение в мировом чате на языке своей расы". Используйте языки, которые понимают все члены фракции.',
			'Interface\\Icons\\Spell_Magic_PolymorphRabbit',
			30,
			3
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_LANG][2] = "Race language" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_REAL] = 
		{
			720,
			'Торговля за реальные средства',
			'Торговля за реальные средства',
			'Нарушение П/П 2.4: "Запрещается торговля игровыми ценностями за реальные средства в обход торговой площадки". Воспользуйтесь услугами сайта.',
			'Interface\\Icons\\Spell_Fire_LavaSpawn',
			-1,
			1
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_REAL][2] = "Real money trading" end
	--		 =============================================================================================================================================
	Rules.MuteList[Rules.Variables.MUTE_GOLD] = 
		{
			720,
			'Торговля персонажами за золото',
			'Торговля персонажами за золото',
			'Нарушение П/П 2.4.1: "Запрещается торговля персонажами за игровые ценности, в том числе за золото". Повторное нарушение приведёт к бану.',
			'Interface\\Icons\\Spell_Holy_Stoicism',
			-1,
			1
		};
	if GetLocale() ~= "ruRU" then Rules.MuteList[Rules.Variables.MUTE_GOLD][2] = "Trading characters for gold" end
		
	return Rules
end



function SMARTM_Rules:GetTime(ReasonID)
	if (ReasonID >= 1) and (ReasonID <= #self.MuteList) then
		return self.MuteList[ReasonID][1]
	else
		return 0
	end
end



function SMARTM_Rules:GetCaption(ReasonID)
	if (ReasonID >= 1) and (ReasonID <= #self.MuteList) then
		return self.MuteList[ReasonID][2]
	else
		return ''
	end
end



function SMARTM_Rules:GetShortReason(ReasonID)
	if (ReasonID >= 1) and (ReasonID <= #self.MuteList) then
		return self.MuteList[ReasonID][3]
	else
		return ''
	end
end



function SMARTM_Rules:GetLongReason(ReasonID)
	if (ReasonID >= 1) and (ReasonID <= #self.MuteList) then
		return self.MuteList[ReasonID][4]
	else
		return ''
	end
end



function SMARTM_Rules:GetIcon(ReasonID)
	if (ReasonID >= 1) and (ReasonID <= #self.MuteList) then
		return self.MuteList[ReasonID][5]
	else
		return ''
	end
end